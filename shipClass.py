import pygame
from enum import Flag, auto

class MovementType(Flag):
    STOP = auto()
    LEFT = auto()
    RIGHT = auto()
    UP = auto()
    DOWN = auto()

class Ship():
    def __init__(self, screen):
        self.screen = screen
        self.image = pygame.image.load("images/ship.bmp")
        self.rect = self.image.get_rect()
        self.screen_rect = screen.get_rect()
        self.rect.centerx = self.screen_rect.centerx
        self.rect.bottom = self.screen_rect.bottom
        self.moving_type = MovementType.STOP
        self.accelerate = 1
        self.mouse_pos = [self.rect.centerx, self.rect.centery]

    def blit(self):
        self.screen.blit(self.image, self.rect)

    def updatePosition(self):
        self.__setMovementType()
        if self.moving_type & MovementType.LEFT:
            self.rect.centerx -= 1 * self.accelerate
        if self.moving_type & MovementType.RIGHT:
            self.rect.centerx += 1 * self.accelerate
        if self.moving_type & MovementType.UP:
            self.rect.centery -= 1 * self.accelerate
        if self.moving_type & MovementType.DOWN:
            self.rect.centery += 1 * self.accelerate

    def __setMovementType(self):
        if self.rect.centerx > self.mouse_pos[0]:
            self.moving_type |= MovementType.LEFT
        if self.rect.centerx < self.mouse_pos[0]:
            self.moving_type |= MovementType.RIGHT
        if self.rect.centery > self.mouse_pos[1]:
            self.moving_type |= MovementType.UP         
        if self.rect.centery < self.mouse_pos[1]:
            self.moving_type |= MovementType.DOWN

        if self.rect.left <= 0 or self.rect.centerx <= self.mouse_pos[0]:
            self.moving_type &= ~MovementType.LEFT
        if self.rect.right >= self.screen_rect.right or self.rect.centerx >= self.mouse_pos[0]:
            self.moving_type &= ~MovementType.RIGHT
        if self.rect.top <= 0 or self.rect.centery <= self.mouse_pos[1]:    
            self.moving_type &= ~MovementType.UP
        if self.rect.bottom >= self.screen_rect.bottom or self.rect.centery >= self.mouse_pos[1]:
            self.moving_type &= ~MovementType.DOWN

        if self.moving_type & MovementType.LEFT or \
            self.moving_type & MovementType.RIGHT or \
                self.moving_type & MovementType.UP or \
                    self.moving_type & MovementType.DOWN :
                        self.moving_type &= ~MovementType.STOP
        else : 
            self.moving_type |= MovementType.STOP
        

    def setPosDirect(self, mouse_pos):
        self.mouse_pos = mouse_pos           
        