import sys
import pygame
from shipClass import MovementType

def check_events(ship):
    for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()

            keydown_events(event, ship)            
            keyup_events(event,ship)

            if event.type == pygame.MOUSEMOTION:
                if pygame.mouse.get_focused():
                    ship.setPosDirect(pygame.mouse.get_pos())

def keydown_events(event, ship):
    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_RIGHT:
            ship.moving_type |= MovementType.RIGHT
        if event.key == pygame.K_LEFT:
            ship.moving_type |= MovementType.LEFT
        if event.key == pygame.K_UP:
            ship.moving_type |= MovementType.UP
        if event.key == pygame.K_DOWN:
            ship.moving_type |= MovementType.DOWN
        
def keyup_events(event, ship):
    if event.type == pygame.KEYUP:
        if event.key == pygame.K_RIGHT:
            ship.moving_type &= ~MovementType.RIGHT
        if event.key == pygame.K_LEFT:
            ship.moving_type &= ~MovementType.LEFT
        if event.key == pygame.K_UP:
            ship.moving_type &= ~MovementType.UP
        if event.key == pygame.K_DOWN:
            ship.moving_type &= ~MovementType.DOWN

def update_screen(ai_settings, screen, ship):
    screen.fill(ai_settings.bg_color)
    ship.blit()
    pygame.display.flip()